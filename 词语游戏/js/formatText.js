import { textArr } from './cy.js'


console.log('textArr', textArr);
console.log(textArr.length);
const newTextArr = [...new Set(textArr)]
console.log(newTextArr.length);

const objArr = []
for (let i = 0; i < newTextArr.length; i++) {
    const text = newTextArr[i]
    if (text.length === 2) {
        const ta = text.slice('')
        if (ta[0] !== ta[1]) {
            // 找到一个跟第一位一样的和第二位跟第一位一样的   组成3个词组 比如：放：开放 放开 奔放
            const obj = {
                c: ta[0]
            }
            let flag = true
            let rflag = true
            newTextArr.forEach((na) => {
                const naArr = na.slice('');
                if (na[0] !== na[1]) {
                    if (na !== text) {
                        if (naArr[0] === ta[0] && rflag) {
                            obj.r = naArr[1]
                            rflag = false
                        } else if (naArr[1] === ta[0] && flag) {
                            obj.l = naArr[0]
                            flag = false
                        }
                        if (naArr[1] === ta[0] && obj.l !== naArr[0]) {
                            obj['t'] = naArr[0]
                        }
                    }
                }
            })
            if (obj.r && obj.l && obj.t) {
                objArr.push(obj)
            }

        }

    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

const template = (item) => (`
<ul id="word">
<li></li>
<li class="t box">${item.t}</li>
<li></li>
<li class="l box">${item.l}</li>
<li class="c box" id="c">???</li>
<li class="r box">${item.r}</li>          
</ul>`)
console.log('objArr', objArr);

let interval = null;
let thatWord = {}
let index = 0

var stopTime = ()=>{
    clearInterval(interval)
    interval = null;
}

const time = document.querySelector('.time');
let timeNumber = 120
const changeTime = () => {
    if (time) {
        interval = setInterval(() => {
            timeNumber -= 1
            time.innerText = timeNumber
            if (timeNumber <= 0) {
                stopTime()
            }
        }, 1000)
    }
}


const getWord = () => {
    index = getRandomInt(0, objArr.length - 1);
    const wordItem = objArr[index];
    thatWord = wordItem;
    const main = document.querySelector('#textMain');
    if (main) {
        main.innerHTML = template(wordItem)
    }
    stopTime()
    changeTime()
}

getWord();


const answerBtn = document.querySelector('#answer');
const nextBtn = document.querySelector('#next');
if (answerBtn && nextBtn) {
    answerBtn.onclick = () => {
        const c = document.querySelector('#c');
        if (c) {
            c.innerText = thatWord.c;
            stopTime()
        }
    }
    nextBtn.onclick = () => {
        timeNumber = 120;
        getWord();
    }
}

const scoreDiv = document.querySelector('.score');
if(scoreDiv) {
    scoreDiv.onclick = (e)=>{
        if(e.target.nodeName==='LI'){
            console.log(e.target.innerText);
            try {
                const n = Number(e.target.innerText||'0');
                console.log('nnn',n);
                e.target.innerHTML = n+1
            } catch (error) {
                
            }
        }
    }
}