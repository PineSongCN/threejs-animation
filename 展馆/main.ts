import { scene } from '../src/ts/scene'
import { renderer } from '../src/ts/renderer'
import { camera } from '../src/ts/camera'
import { hemiLight, dirLight } from '../手柄控制摄像头跟踪/light'
import { loadFbx, loadGltf } from '../src/ts/loaders'
import * as THREE from 'three'
import { HandleAnimation } from '../src/ts/animation'
import { HandlePointerLock } from '../src/ts/PointerLockControls'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

let controls, handleControls, helperGroup = new THREE.Group(), PlaneSize = 1000, textureLoader = new THREE.TextureLoader();
scene.add(...[hemiLight, dirLight])
const helper = new THREE.DirectionalLightHelper(dirLight, 50);
helperGroup.add(helper);
scene.add(helperGroup)

// 控制器位置
const velocity = new THREE.Vector3();
// 控制器角度
const direction = new THREE.Vector3();
// 初始时间
let prevTime = performance.now();
// 方向
let moveForward = false;
let moveBackward = false;
let moveLeft = false;
let moveRight = false;

let player

const init = async () => {
    document.body.appendChild(renderer.domElement);

    const axesHelper = new THREE.AxesHelper(10);
    helperGroup.add(axesHelper)

    window.addEventListener('resize', onWindowResize);

    createObjects()
    loadPlayer()

    // controls = new OrbitControls(camera, renderer.domElement)

    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup', onKeyUp);

}

const loadPlayer = async () => {
    const xbot: any = await loadGltf('../src/assets/models/Xbot.glb')
    player = new HandleAnimation(xbot.scene, xbot.animations)
    player.play('idle')
    scene.add(player.model)
    helperGroup.add(player.createSkeleton());

    handleControls = new HandlePointerLock(xbot.scene, document.body)
    controls = handleControls.controls

    document.body.addEventListener('click', function () {

        controls.lock();

    });
}

// 创建所有需要的元素
async function createObjects() {
    // 创建底板
    // const geometry = new THREE.PlaneGeometry(PlaneSize, PlaneSize);
    // const material = new THREE.MeshLambertMaterial({ color: 0xffffff, side: THREE.DoubleSide });
    // const plane = new THREE.Mesh(geometry, material);
    // plane.receiveShadow = true;
    // textureLoader.load('../src/assets/textures/grid.png', function (texture) {
    //     texture.wrapS = THREE.RepeatWrapping;
    //     texture.wrapT = THREE.RepeatWrapping;
    //     texture.repeat.set(1000, 1000);
    //     plane.material.map = texture;
    //     plane.material.needsUpdate = true;
    // });
    // plane.rotation.set(-0.5 * Math.PI, 0, 0);
    // scene.add(plane)

    const build = await loadFbx('../src/assets/models/first floor.FBX');
    const scale = 0.001 * 1.2
    build.scale.set(scale, scale, scale)
    console.log('build', build);
    build.traverse((child)=>{
        console.log(child);
        
    })
    scene.add(build)

}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}
function render() {
    player && player.upDate(camera)

    handleControls && handleControls.updateControls(moveForward)
    // 更新控制器
    renderer.render(scene, camera);
}

const onKeyUp = function (event) {

    switch (event.code) {

        case 'ArrowUp':
        case 'KeyW':
            if (moveForward && controls.isLocked) {
                player.fadeToAction('idle')
            }
            moveForward = false;
            break;
    }


};

const onKeyDown = function (event) {
    switch (event.code) {
        case 'ArrowUp':
        case 'KeyW':
            if (!moveForward && controls.isLocked) {
                player.fadeToAction('walk')
            }
            moveForward = true;
            break;

    }
}

// 循环渲染
function animate() {

    requestAnimationFrame(animate);
    render();
}

init();
animate();