import { RepeatWrapping, AxesHelper, LoopOnce, Group, AnimationMixer, DirectionalLightHelper, SkeletonHelper, Box3, Matrix4, DoubleSide, PlaneGeometry, MeshBasicMaterial, Clock, TextureLoader, ColorRepresentation, Quaternion, Vector3, WebGLRenderer, Mesh, Material, BoxGeometry, Color, MeshPhongMaterial, Object3D, Euler, Vector4, Scene, Vector2 } from 'three'
import * as THREE from 'three'
import { plane, sphere } from './js/createFloor'
let Ammo = (window as any).Ammo
import { dirLight, hemiLight } from './js/light'
import { renderer as rendererTS } from '../src/ts/renderer'
import { camera as cameraTS } from '../src/ts/camera'
import { scene as sceneTS } from '../src/ts/scene'
import { loadFbx, loadGltf } from '../src/ts/loaders';
import { HandleAnimation } from '../src/ts/animation'
import { createRigidBody } from '../src/ts/rigidBody'
import { initPhysics, updatePhysics, physicsWorld, dispatcher } from '../src/ts/physics'
import { HandlePointerLock } from './js/PointerLockControls';
import { getAnimations, animations } from './js/animations';
import { onKeyDown, onKeyUp, dir, speed } from './js/watchKey';
const helperGroup = new Group()
const playerClock = new Clock();
let camera = cameraTS, Player: any,enemy: any, controls: any, scene = sceneTS, renderer = rendererTS, Handles,enemyAnimation, handleControls

// 辅助线
const axesHelper = new AxesHelper(100);
helperGroup.add(axesHelper);
scene.add(helperGroup);

const rigidBodies: Object3D[] = [];

Ammo().then(function (AmmoLib: any) {
    Ammo = AmmoLib;
    (window as any).Ammo = Ammo
    init();
    animate();
});


const init = async () => {
    // 创建场景
    initGraphics();
    // 创建物理引擎
    initPhysics();
    // 创建物理物体
    await createObjects();
    // 创建主角
    initPlayer()
    // 创建敌人
    initEnemy()
}

function initGraphics() {
    scene.add(...[hemiLight, dirLight])

    document.body.appendChild(renderer.domElement);

    window.addEventListener('resize', onWindowResize);

}

// 创建所有需要的元素
async function createObjects() {
    await getAnimations('../src/assets/models/动作合集/Idle.fbx', 'idle')
    await getAnimations('../src/assets/models/动作合集/Walking.fbx', 'walk')
    await getAnimations('../src/assets/models/动作合集/WalkLeft.fbx', 'walk-left')
    await getAnimations('../src/assets/models/动作合集/WalkRight.fbx', 'walk-right')
    await getAnimations('../src/assets/models/动作合集/WalkBack.fbx', 'walk-back')
    await getAnimations('../src/assets/models/动作合集/Jump.fbx', 'jump')
    await getAnimations('../src/assets/models/动作合集/Running.fbx', 'run')
    await getAnimations('../src/assets/models/动作合集/Running_Backward.fbx', 'run-back')
    await getAnimations('../src/assets/models/动作合集/Running_left.fbx', 'run-left')
    await getAnimations('../src/assets/models/动作合集/Running_right.fbx', 'run-right')


    // 引用地板并加入物理世界
    const { object, body } = createRigidBody(plane, true, false, null, null)
    object.name = 'floor'
    object.receiveShadow = true
    physicsWorld.addRigidBody(body);
    setPointerBttVec(object, body)
    rigidBodies.push(object)
    body.setRollingFriction(10)

    scene.add(object)
}

const initEnemy = async () => {
    let i = 0
    while (i < 1) {
         enemy = await loadFbx('../src/assets/models/动作合集/Mutant_Run.fbx')
        const { object, body } = createRigidBody(enemy, true, true, new Vector3(400, 1, -10000 + 400), null, undefined, undefined)
        object.userData.PhysUpdate = true
        object.userData.rotateLock = true
        object.userData.positionLock = ['', '', '']
        object.lookAt(Player.position)
        console.log('enemy',enemy);
        
        if (animations && animations.length !== 0) {
            // 绑定主角动画
            enemyAnimation = new HandleAnimation(object, enemy.animations)
            // 设置主角初始化动作
            enemyAnimation.play('mixamo.com')
        }
        // 将主角添加至物理世界
        physicsWorld.addRigidBody(body);

        // 添加主角至刚体系统
        rigidBodies.push(object)
        scene.add(object)
        i++
    }

}
const initPlayer = async () => {
    // 创建主角并加入物理世界
    Player = await loadFbx('../src/assets/models/动作合集/idle.fbx')
    const { object, body } = createRigidBody(Player, true, true, new Vector3(0, 1, -10000), null, undefined, undefined)
    object.name = 'Player'
    object.userData.PhysUpdate = true
    object.userData.rotateLock = true
    object.userData.positionLock = ['', '', '']
    object.traverse(function (object) {
        if (object.isMesh) object.castShadow = true;
    });

    if (animations && animations.length !== 0) {
        // 绑定主角动画
        Handles = new HandleAnimation(object, animations)
        // 设置主角初始化动作
        Handles.play('idle')
        Handles.once(['jump'])
        // 渲染主角骨骼
        helperGroup.add(Handles.createSkeleton());
    }

    // 将主角添加至物理世界
    physicsWorld.addRigidBody(body);

    setPointerBttVec(object, body);
    // 添加主角至刚体系统
    rigidBodies.push(object)
    scene.add(object)
    handleControls = new HandlePointerLock(object, document.body)
    controls = handleControls.controls

    renderer.domElement.addEventListener('click', function () {
        controls.lock();
    });


    controls.addEventListener('change', function () {

    });
    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup', onKeyUp);

}

// 设置物体指针
const setPointerBttVec = (object, body) => {
    const btVecUserData = new Ammo.btVector3(0, 0, 0);
    btVecUserData.threeObject = object;
    body.setUserPointer(btVecUserData);
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}



// 循环渲染
function animate() {
    requestAnimationFrame(animate);
    render();
}

const changeAnimate = (name: string) => {
    if (Handles.thatState !== name) Handles.fadeToAction(name)
}

// 渲染函数
function render() {
    const dt = playerClock.getDelta();
    // 更新物理世界
    updatePhysics(dt, rigidBodies)
    // 更新动画

    if (Handles) {
        Handles.upDate(camera, 308, 50)
    }
    // 更新控制器
    // handleControls && handleControls.updateControls()
    if (controls?.isLocked) {
        if (enemyAnimation) {
            enemyAnimation.upDate()
        }
        const {x,z} = Player.position.clone().normalize()
        enemy.lookAt(Player.position)
        
        enemy.userData.physicsBody.setLinearFactor(new Ammo.btVector3(x*100,1,z*100))
        // enemy.userData.physicsBody.setLinearVelocity(new Ammo.btVector3(Player.position.clone().normalize().x,1,Player.position.clone().normalize().z))

        const rotate = new THREE.Vector3()
        controls.getDirection(rotate)
        const a = controls.getObject().rotation
        const d = dir.clone()
        d.applyEuler(a).multiplyScalar(60);
        dirLight.position.set(d.x, 1000, d.z)
        controls.getObject().userData.physicsBody.setLinearVelocity(new Ammo.btVector3(d.x, 0, d.z))
        if (dir.z > 0) changeAnimate(speed > 1 ? 'run' : 'walk')
        else if (dir.z < 0) changeAnimate(speed > 1 ? 'run-back' : 'walk-back')
        else if (dir.x > 0) changeAnimate(speed > 1 ? 'run-left' : 'walk-left')
        else if (dir.x < 0) changeAnimate(speed > 1 ? 'run-right' : 'walk-right')
        else if (dir.length() === 0) changeAnimate('idle')
    }
    rayRigidBodyCheck()

    renderer.render(scene, camera);
}

// 碰撞检测
const rayRigidBodyCheck = () => {
    // 物理世界的物体数量,在initPhysics初始化物理引擎注册的
    for (let i = 0, il = dispatcher.getNumManifolds(); i < il; i++) {

        const contactManifold = dispatcher.getManifoldByIndexInternal(i);

        const rb0 = Ammo.castObject(contactManifold.getBody0(), Ammo.btRigidBody);
        const rb1 = Ammo.castObject(contactManifold.getBody1(), Ammo.btRigidBody);

        const threeObject0 = Ammo.castObject(rb0.getUserPointer(), Ammo.btVector3).threeObject;
        const threeObject1 = Ammo.castObject(rb1.getUserPointer(), Ammo.btVector3).threeObject;

        const n = threeObject0?.name
        const n1 = threeObject1?.name

        if (n1 === 'Player' && n === 'floor') {
            // console.log('踢球');
            // isJump = j
        }

    }
}