import * as THREE from 'three'
import { loadFbx } from '../../src/ts/loaders'
export let group = new THREE.Group()
let srcs = [
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Birch_Tree_05.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Birch_Tree_06.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Daffodil_03.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Forest_Mountain_Moss_01.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Forest_Mountain_Moss_02.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Grass_11.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Grass_15.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Hyacinth_04.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Lake_Ground_04.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Meadow_07.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Meadow_08.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Rock_Moss_Grown_09.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Meadow_Path_05.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Mushroom_Fantasy_Orange_09.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Mushroom_Fantasy_Orange_10.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Mushroom_Fantasy_Purple_05.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Mushroom_Fantasy_Purple_08.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Rock_Moss_Grown_11.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Rock_Pile_Forest_Moss_05.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Sunflower_04.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Tree_02.fbx',
    '../../src/assets/models/PP_Free_Forest_Unique fbx files/PP_Tree_10.fbx',
]

for (let i = 0; i < srcs.length; i++) {
    loadFbx(srcs[i]).then((obj: any) => {
        group.add(obj.children?.[0])
    })
}