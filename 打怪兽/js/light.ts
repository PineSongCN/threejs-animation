import * as THREE from 'three'
export const hemiLight = new THREE.HemisphereLight(0xffffff, 0x000000);
hemiLight.position.set(-500, 500, 0);

export const dirLight = new THREE.DirectionalLight(0xffffff,3);
dirLight.position.set(300, 100, 100);
dirLight.castShadow = true;
dirLight.shadow.camera.top = 1000;
dirLight.shadow.camera.bottom = - 1000;
dirLight.shadow.camera.left = - 1000;
dirLight.shadow.camera.right = 1000;
dirLight.shadow.camera.near = 0.1;
dirLight.shadow.camera.far = 40000;
