import * as THREE from 'three'

export const dir = new THREE.Vector3()
export let isJump = false
export let speed = 1
let speed2 = 4
export const onKeyUp = function (event: any) {
    switch (event.code) {

        case 'KeyW':
            dir.setZ(0)
            break;
        case 'KeyS':
            dir.setZ(0)
            break;
        case 'KeyA':
            dir.setX(0)
            break;
        case 'KeyD':
            dir.setX(0)
            break;
        case 'ShiftLeft':
            speed = 1
            break;
        case 'Space':
            isJump = false
            break;
    }
};

export const onKeyDown = function (event: any) {
    switch (event.code) {
        case 'KeyW':
            dir.setZ(1 * speed)
            dir.setX(0)
            break;
        case 'KeyS':
            dir.setZ(-1 * speed)
            dir.setX(0)
            break;
        case 'KeyA':
            dir.setX(1 * speed)
            dir.setZ(0)
            break
        case 'KeyD':
            dir.setX(-1 * speed)
            dir.setZ(0)
            break;
        case 'ShiftLeft':
            speed = speed2
            break;
        case 'Space':
            isJump = true
            break;
    }
}
export const setIsJump = (s: boolean)=>{
    isJump = s
}