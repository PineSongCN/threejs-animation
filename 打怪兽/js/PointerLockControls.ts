import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls.js';
import * as THREE from 'three'
import { dir } from './watchKey.js'

export class HandlePointerLock {

    // 控制器位置
    velocity = new THREE.Vector3();
    // 控制器角度
    direction = new THREE.Vector3();
    // 初始时间
    prevTime = performance.now();
    controls: PointerLockControls
    Ammo: any
    constructor(model: THREE.Camera, document: HTMLElement) {
        this.controls = new PointerLockControls(model, document);
        this.controls.maxPolarAngle = Math.PI * 0.5
        this.controls.minPolarAngle = Math.PI * 0.5

        this.Ammo = (window as any).Ammo
    }
    updateControls() {
        if (this.controls.isLocked) {
            

        }
    }
}