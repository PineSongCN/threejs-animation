import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { scene } from '../src/ts/scene'
import { renderer } from '../src/ts/renderer'
import { camera } from '../src/ts/camera'
import { ambientLight, light } from '../src/ts/light'
import { loadFbx, loadObj } from '../src/ts/loaders'
import * as THREE from 'three'
let controls, helperGroup = new THREE.Group(), PlaneSize = 1000, textureLoader = new THREE.TextureLoader();
scene.add(...[ambientLight, light])
const helper = new THREE.DirectionalLightHelper( light, 50 );
helperGroup.add( helper );

scene.add(helperGroup)

// 主角当前动画 
let playerActiveAction: any = null
// 主角上一次动画
let previousAction: any = null
// 主角的动画器
let playerMixer: THREE.AnimationMixer
// 更新主角时钟
const playerClock = new THREE.Clock();



const init = async () => {
    document.body.appendChild(renderer.domElement);

    controls = new OrbitControls(camera, renderer.domElement)

    const axesHelper = new THREE.AxesHelper(100);
    helperGroup.add(axesHelper)

    window.addEventListener('resize', onWindowResize);

    loadMap()

    loadSonic()


}

const loadSonic = async ()=>{
    const map = await loadFbx('../src/assets/models/EXPANSION_SonicBuilder_01/fbx/display_idle.fbx')
    console.log(map);
    map.traverse((m)=>{
        console.log(m.animations.length);
        
    })
    // scene.add(map)
}
const loadMap = async ()=>{
    const map = await loadFbx('../src/assets/models/EXPANSION_SonicBuilder_01/fbx/map_transforming.fbx')
    console.log(map);
    map.traverse((m)=>{
        console.log(m.name);
        
    })
    const animations = map.animations
    playerMixer = new THREE.AnimationMixer(map);

    const clip = animations[0];
    const action = playerMixer.clipAction(clip);

    action.clampWhenFinished = true;

    playerActiveAction = action

    playerActiveAction
        .reset()
        .setEffectiveTimeScale(1)
        .setEffectiveWeight(1)
        .fadeIn(1)
        .play();

    scene.add(map)
    
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}
function render() {
    const dt = playerClock.getDelta();
    // if (playerMixer) playerMixer.update(dt);
    // 更新控制器
    renderer.render(scene, camera);
}

// 循环渲染
function animate() {
    
    requestAnimationFrame(animate);
    render();
}

init();
animate();