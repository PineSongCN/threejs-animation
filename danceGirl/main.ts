import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { scene } from '../src/ts/scene'
import { renderer } from '../src/ts/renderer'
import { camera } from '../src/ts/camera'
import { ambientLight, light } from '../src/ts/light'
import { Group, AxesHelper, PlaneGeometry, MeshBasicMaterial, Vector3, Box3, AnimationMixer, Clock, DoubleSide, Mesh, TextureLoader, RepeatWrapping } from 'three';
import { loadFbx, loadObj } from '../src/ts/loaders'
let controls, helperGroup = new Group(), PlaneSize = 1000, textureLoader = new TextureLoader();
scene.add(...[ambientLight, light])
scene.add(helperGroup)

// 主角当前动画 
let playerActiveAction: any = null
// 主角上一次动画
let previousAction: any = null
// 主角的动画器
let playerMixer: AnimationMixer
// 更新主角时钟
const playerClock = new Clock();


const init = async () => {
    document.body.appendChild(renderer.domElement);

    controls = new OrbitControls(camera, renderer.domElement)

    const axesHelper = new AxesHelper(100);
    helperGroup.add(axesHelper)

    createPlane()
    // loadBody()
    loadDanceGirl()

    window.addEventListener('resize', onWindowResize);

}

async function loadDanceGirl() {
    let body = await loadFbx('../src/assets/models/跳舞的女孩.fbx')
    console.log('body', body);
    const animations = body.animations
    playerMixer = new AnimationMixer(body);

    const clip = animations[0];
    const action = playerMixer.clipAction(clip);

    action.clampWhenFinished = true;

    playerActiveAction = action

    playerActiveAction
        .reset()
        .setEffectiveTimeScale(1)
        .setEffectiveWeight(1)
        .fadeIn(1)
        .play();

        body.scale.set(0.1,0.1,0.1)
    const box = new Box3()
    box.expandByObject(body);
    const size = new Vector3();
    box.getSize(size)
    console.log();
    body.position.setY(size.y / 2)

    scene.add(body)
}
async function loadBody() {
    let body = await loadObj('../src/assets/models/战士.obj');
    console.log('body', body);
    scene.add(body)

}
// 创建所有需要的元素
async function createPlane() {
    // 创建底板
    const geometry = new PlaneGeometry(PlaneSize, PlaneSize);
    const material = new MeshBasicMaterial({ color: 0xffffff, side: DoubleSide });
    const plane = new Mesh(geometry, material);
    textureLoader.load('../src/assets/textures/hardwood2_diffuse.jpg', function (texture) {
        texture.wrapS = RepeatWrapping;
        texture.wrapT = RepeatWrapping;
        texture.repeat.set(100, 100);
        plane.material.map = texture;
        plane.material.needsUpdate = true;
    });
    plane.rotation.set(0.5 * Math.PI, 0, 0);
    scene.add(plane)
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}
function render() {
    const dt = playerClock.getDelta();
    if (playerMixer) playerMixer.update(dt);
    // 更新控制器
    renderer.render(scene, camera);
}

// 循环渲染
function animate() {
    requestAnimationFrame(animate);
    render();
}

init();
animate();