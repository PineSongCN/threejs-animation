import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import * as THREE from 'three'
import { renderer as rendererTS } from '../src/ts/renderer'
import { camera as cameraTS } from '../src/ts/camera'
import { scene as sceneTS } from '../src/ts/scene'
import { loadFbx, loadGltf, loadObjMtl, loadTexture } from '../src/ts/loaders'
import { ambientLight, light } from '../src/ts/light'

export const helperGroup = new THREE.Group()
let camera = cameraTS, controls: any, scene = sceneTS.clone(), renderer = rendererTS
// 辅助线
const axesHelper = new THREE.AxesHelper(100);
scene.add(axesHelper);
scene.add(helperGroup);
animate();
init()
// ray()
// function ray() {
//     let mouse = new THREE.Vector2(); //鼠标位置
//     var raycaster = new THREE.Raycaster();
//     window.addEventListener("click", (event) => {
//         mouse.x = (event.clientX / document.body.offsetWidth) * 2 - 1;
//         mouse.y = -(event.clientY / document.body.offsetHeight) * 2 + 1;
//         raycaster.setFromCamera(mouse, camera);
//         var raylist = raycaster.intersectObjects(group.children);
//         const meth = raylist[0];
//         if (meth) {
//             meth.object.material.color = new THREE.Color("#ff0000");
//             console.log(meth.object.name);
//         }
//     });
// }

async function init() {
    camera.position.set(500, 500, 500)
    camera.updateMatrixWorld()
    document.body.appendChild(renderer.domElement);
    controls = new OrbitControls(camera, renderer.domElement)
    scene.add(light)
    const texture = await loadTexture('../src/assets/textures/bg.jpeg')
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(1, 1);

    scene.background = texture


    const diqiu: any = await loadFbx('../src/assets/models/diqiu.fbx')
    
    const diqiuTexture = await loadTexture('../src/assets/textures/diqiu.jpeg')
    diqiu.children[0].material.map = diqiuTexture
    diqiuTexture.wrapS = THREE.RepeatWrapping;
    diqiuTexture.wrapT = THREE.RepeatWrapping;
    diqiuTexture.repeat.set(1, 1);
    console.log(diqiu)
    scene.add(diqiu.children[0])

}

// 循环渲染
function animate() {
    requestAnimationFrame(animate);
    render();
}

// 渲染函数
function render() {
    renderer.render(scene, camera);
}
