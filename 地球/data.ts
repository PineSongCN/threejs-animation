import { Vector3 } from "three";

export const data = [
    {
        vector: new Vector3(2.178019931426133,
            3.892510720775519,
            4.571573730411999,),
        country: '日本',
        mapUrl: '../src/assets/image/全景直播/DM_20230428094437_030.PNG',
        iconUrl: '../src/assets/image/全景直播/DM_20230428094437_071.PNG'
    },
    { "vector": { "x": 1.8528925406382806, "y": -2.561329496584602, "z": 5.632220170309556 }, "country": "澳大利亚", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_049.PNG", "iconUrl": "../src/assets/image/全景直播/DM_20230428094437_090.PNG" },
    { "vector": { "x": -0.6715928905231785, "y": -0.85248353585649, "z": 6.346495405904364 }, "country": "印度尼西亚", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_033.PNG", "iconUrl": "../src/assets/image/全景直播/DM_20230428094437_087.PNG" },
    { "vector": { "x": -1.2189344848438557, "y": 0.2421500573253616, "z": 6.303708486398252 }, "country": "新加坡", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_052.PNG", "iconUrl": "../src/assets/image/全景直播/DM_20230428094437_093.PNG" },
    { "vector": { "x": -1.421930122952471, "y": 1.9686032349294371, "z": 5.926892403550216 }, "country": "泰国", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_047.PNG", "iconUrl": "../src/assets/image/全景直播/DM_20230428094437_088.PNG" },
    { "vector": { "x": 1.1950339810801354, "y": 3.952507729521826, "z": 4.870659150502724 }, "country": "韩国", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_046.PNG", "iconUrl": "../src/assets/image/全景直播/DM_20230428094437_086.PNG" },
    { "vector": { "x": -1.435812227965829, "y": 0.4844192536262843, "z": 6.241259890190982 }, "country": "马来西亚", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_044.PNG", "iconUrl": "../src/assets/image/全景直播/DM_20230428094437_084.PNG" },
    { "vector": { "x": -3.070116286947089, "y": 5.582151981407805, "z": 0.037569201755330316 }, "country": "芬兰", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_042.PNG", "iconUrl": "../src/assets/image/全景直播/DM_20230428094437_082.PNG" },
    { "vector": { "x": -2.2913118484926986, "y": 5.512521221748865, "z": 2.2251919876043385 }, "country": "俄罗斯", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_032.PNG", "iconUrl": "../src/assets/image/全景直播/DM_20230428094437_072.PNG" },
    { "vector": { "x": -3.1361126146038236, "y": 5.3014886262588545, "z": -1.6338963163761153 }, "country": "英国", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_034.PNG", "iconUrl": "../src/assets/image/全景直播/DM_20230428094437_073.PNG" },
    { "vector": { "x": -0.8554277658184936, "y": 3.1406944047367262, "z": 5.501630743180464 }, "country": "中国大熊猫保护研究中心", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_027.PNG", "iconUrl": "../", style: { width: '120px' } },
    { "vector": { "x": -0.30674398278721315, "y": 2.739959097188597, "z": 5.771830845839392 }, "country": "成都大熊猫繁育研究中心", "mapUrl": "../src/assets/image/全景直播/DM_20230428094437_028.PNG", "iconUrl": "../", style: { width: '120px' } }
]