import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import * as THREE from 'three'
import { AmbientLight } from 'three'
import { loadFbx, loadGltf, loadObjMtl, loadTexture } from '../src/ts/loaders'
import { CSS2DRenderer } from 'three/examples/jsm/renderers/CSS2DRenderer'
import { LineMaterial } from "three/examples/jsm/lines/LineMaterial.js";
import { Line2 } from "three/examples/jsm/lines/Line2.js";
import { WebGLRenderer } from 'three'
import { HandleAnimation } from '../src/ts/animation'
import { LineGeometry } from "three/examples/jsm/lines/LineGeometry.js";
import { PerspectiveCamera, Vector3, Vector2 } from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'

import TWEEN from '@tweenjs/tween.js'
import { Scene } from "three";
import _ from 'lodash'

let scene, camera, renderer, ambientLight, labelRenderer, controls

let group = new THREE.Group()
let table, person
const content: any = document.querySelector('#three')
const width = content.offsetWidth
const height = content.offsetHeight
init()
ray()

async function init() {
    // 场景
    scene = new Scene();
    scene.add(group)
    scene.add(new THREE.AxesHelper(5))

    // 镜头
    camera = new PerspectiveCamera(30, width / height, 0.2, 2000000);
    // const cameraPos = new Vector3(21.8, 11, -23);
    const cameraPos = new Vector3(-20.7, 9.3, 24);
    camera.position.copy(cameraPos)

    // 渲染器
    renderer = new WebGLRenderer({ alpha: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(width, height);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.needsUpdate = true
    renderer.setClearAlpha(1);
    // renderer.setClearColor( 0x000000, 0 );
    renderer.setClearColor(0xffffff, 0);
    renderer.domElement.style.filter = 'saturate(60%) sepia(25%)'
    content.appendChild(renderer.domElement);

    labelRenderer = new CSS2DRenderer();
    labelRenderer.setSize(width, height);
    labelRenderer.domElement.style.position = "absolute";
    labelRenderer.domElement.style.top = "0";
    labelRenderer.domElement.style.pointerEvents = "none";
    content.appendChild(labelRenderer.domElement);
    controls = new OrbitControls(camera, renderer.domElement)

    let light = initLight()
    light.position.copy(camera.position.clone())
    scene.add(light)

    controls.addEventListener('change', () => {
        const pos = camera.position.clone().setLength(4).setY(4)
        light.position.copy(pos)
        light.updateMatrix()
    })
    ambientLight = new AmbientLight(0xffffff, 0.4);
    scene.add(ambientLight)
    animate();
    loadMesh()

}

async function loadMesh() {
    const file2 = await loadObjMtl('./model/uploads_files_3825299_Low+poly+bedroom_Obj/Scene/Low poly bedroom.obj', './model/uploads_files_3825299_Low+poly+bedroom_Obj/Scene/Low poly bedroom.mtl')
    file2.traverse((mesh: any) => {
        mesh.castShadow = true
        mesh.receiveShadow = true
    })
    const size = new THREE.Vector3()
    getSize(file2, size)
    const { x: x2, y: y2, z: z2 } = size

    file2.position.set(size.x / 2, 0, -size.z / 2)
    group.add(file2)

    const file1 = await loadObjMtl('./model/uploads_files_3853084_Kitchen_Obj/Kitchen.obj', './model/uploads_files_3853084_Kitchen_Obj/Kitchen.mtl')
    file1.traverse((mesh: any) => {
        mesh.castShadow = true
        mesh.receiveShadow = true
    })

    const size1 = new THREE.Vector3()
    getSize(file1, size1)
    const { x: x1, y: y1, z: z1 } = size1
    const xScale = x2 / x1
    const yScale = y2 / y1
    const zScale = z2 / z1
    console.log(xScale, yScale, zScale)
    file1.position.set(-size1.x / 2 * xScale - 0.2, 0 - 0.2, -size1.z / 2 - 0.8)
    file1.scale.set(xScale, yScale, zScale)

    group.add(file1)

    const file3 = await loadObjMtl('./model/uploads_files_3844681_Another+bedroom_Obj/Another bedroom.obj', './model/uploads_files_3844681_Another+bedroom_Obj/Another bedroom.mtl')
    file3.traverse((mesh: any) => {
        mesh.castShadow = true
        mesh.receiveShadow = true
    })

    const size3 = new THREE.Vector3()
    getSize(file3, size3)
    const { x: x3, y: y3, z: z3 } = size3
    // const { x: x2, y: y2, z: z2 } = size
    const x3Scale = x2 / x3
    const y3Scale = y2 / y3
    const z3Scale = z2 / z3
    // console.log(xScale, yScale, zScale)
    file3.position.set(-size3.x / 2 - 0.1, 0 - 0.2, size3.z / 2 + 0.3)
    file3.rotation.y = Math.PI
    file3.scale.set(x3Scale - 0.05, y3Scale, z3Scale)

    group.add(file3)

}

// 循环渲染
function animate() {
    requestAnimationFrame(animate);
    render();
}

// 渲染函数
function render() {
    renderer.render(scene, camera);
    TWEEN && TWEEN.update();
}

function ray() {
    window.addEventListener("click", (event) => {
        const rayList = rayMesh(event, group?.children || [])
        console.log(rayList);
        if (rayList.length !== 0) {
            const obj = rayList[0]?.object
            if (obj) {
                if (obj?.name === 'Wardrobe_1_Cube.019_Wardrobe') {
                    // obj.material.color = new THREE.Color(0xbacbfe)
                }
            }
        }
    });
}
function rayMesh(event, child: any[]) {

    let mouse = new THREE.Vector2(); //鼠标位置
    var rayCaster = new THREE.Raycaster();
    mouse.x = (event.clientX / content.offsetWidth) * 2 - 1;
    mouse.y = -(event.clientY / content.offsetHeight) * 2 + 1;
    rayCaster.setFromCamera(mouse, camera);
    return rayCaster.intersectObjects(child);
}
function getSize(mesh: THREE.Object3D, size: THREE.Vector3) {
    const file2Box3 = new THREE.Box3()
    file2Box3.expandByObject(mesh)
    file2Box3.getSize(size)
}
function initLight(value = 1) {

    const directionalLight = new THREE.DirectionalLight(0xffffff, value);
    directionalLight.castShadow = true; // default false

    directionalLight.shadow.mapSize.width = 5120; // default
    directionalLight.shadow.mapSize.height = 5120; // default
    directionalLight.shadow.camera.near = 0.5; // default
    directionalLight.shadow.camera.far = 500; // default
    return directionalLight
    // scene.add(directionalLight);
}